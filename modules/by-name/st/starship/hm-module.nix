{ lib, ... }:
{
  config.programs.starship = {
    enable = lib.mkDefault true;
    enableFishIntegration = false;
    settings = {
      format = "[$directory](fg:#7eb4f6)$git_branch$git_status$character";
      right_format = "$cmd_duration";
      scan_timeout = 60;
      command_timeout = 3600;
      add_newline = false;

      cmd_duration.format = "[$duration](bold yellow)";

      character = {
        success_symbol = "[\\$](bold green)";
        error_symbol = "[\\$](bold red)";
      };

      directory = {
        truncate_to_repo = false;
        fish_style_pwd_dir_length = 1;
        style = "fg:#7eb4f6";
      };

      git_branch.format = "[$branch](fg:red) ";

      time = {
        style = "fg:green";
        format = "[$time](fg:green)";
        time_format = "%T";
        disabled = true;
      };
    };
  };
}
