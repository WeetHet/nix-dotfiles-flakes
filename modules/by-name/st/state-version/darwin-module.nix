{ lib, ... }:
{
  system.stateVersion = lib.mkDefault 6;
}
