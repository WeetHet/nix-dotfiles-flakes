{ config, lib, ... }:
let
  inherit (lib) types;
  cfg = config.programs.less;
in
{
  options.programs.less = {
    remapColemak = lib.mkOption {
      description = "Remap less keys to colemak";
      type = types.bool;
      default = false;
    };
  };

  config = lib.mkIf (cfg.enable && cfg.remapColemak) {
    programs.less.keys = ''
      e    forw-line
      i    back-line
      k    repeat-search
      \ek  repeat-search-all
      K    reverse-search
      \eK  reverse-search-all
    '';
  };
}
