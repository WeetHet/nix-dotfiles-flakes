{ ... }:
{
  theme = "edge-neon";

  font-family = "FiraCode Nerd Font";
  font-size = "14";

  macos-titlebar-style = "tabs";
  macos-option-as-alt = "true";

  mouse-scroll-multiplier = "3";

  confirm-close-surface = "false";

  font-feature = [
    "-calt"
    "-liga"
    "-dlig"
  ];

  window-height = "34";
  window-width = "100";

  adjust-cell-height = "3";

  cursor-style-blink = "false";

  background-opacity = "0.95";
  background-blur-radius = "10";

  window-padding-color = "extend";
}
