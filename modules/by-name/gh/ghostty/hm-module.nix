{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) types;
  cfg = config.programs.ghostty;
  isDarwin = pkgs.stdenv.hostPlatform.isDarwin;
in
{
  options.programs.ghostty = {
    setTerminfoVar = lib.mkOption {
      description = "Set correct TERMINFO_DIRS and TERMINFO for ghostty (darwin only)";
      type = types.bool;
      default = isDarwin;
    };
  };

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = (cfg.setTerminfoVar -> isDarwin);
        message = "setTerminfoVar is only supported on darwin";
      }
    ];
    programs.ghostty = {
      package =
        if (pkgs.stdenv.hostPlatform.isDarwin && lib.hasAttr "brewCasks" pkgs) then
          pkgs.brewCasks.ghostty
        else
          pkgs.ghostty;

      enableFishIntegration = true;
      settings = import ./settings.nix { };
      themes = import ./themes.nix { };
    };
    home.sessionVariables =
      let
        ghosttyTerminfo = "${config.home.homeDirectory}/Applications/Home Manager Apps/Ghostty.app/Contents/Resources/terminfo";
      in
      lib.optionalAttrs (cfg.setTerminfoVar && pkgs.stdenv.isDarwin) {
        TERMINFO_DIRS = "${ghosttyTerminfo}:\${TERMINFO_DIRS}";
        TERMINFO = lib.mkForce ghosttyTerminfo;
      };
  };
}
