{ ... }:
{
  edge-neon = {
    background = "#2b2d3a";
    foreground = "#c5cdd9";
    palette = [
      # black
      "0=#363a4e"
      "8=#363a4e"
      # red
      "1=#ec7279"
      "9=#ec7279"
      # green
      "2=#a0c980"
      "10=#a0c980"
      # yellow
      "3=#deb974"
      "11=#deb974"
      # blue
      "4=#6cb6eb"
      "12=#6cb6eb"
      # purple
      "5=#d38aea"
      "13=#d38aea"
      # aqua
      "6=#5dbbc1"
      "14=#5dbbc1"
      # white
      "7=#c5cdd9"
      "15=#c5cdd9"
    ];
  };
}
