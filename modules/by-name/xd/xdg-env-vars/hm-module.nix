{ config, lib, ... }:
let
  inherit (config.xdg)
    dataHome
    configHome
    cacheHome
    stateHome
    ;
in
{
  imports = [ { config.xdg.enable = lib.mkDefault true; } ];
  config = lib.mkIf config.xdg.enable {
    home.sessionVariables = {
      HISTFILE = "${stateHome}/bash/history";
      LESSHISTFILE = "${cacheHome}/less/history";
      TERMINFO = "${dataHome}/terminfo";

      DOCKER_CONFIG = "${configHome}/docker";

      IPYTHONDIR = "${configHome}/ipython";
      JUPYTER_CONFIG_DIR = "${configHome}/jupyter";
      MPLCONFIGDIR = "${configHome}/matplotlib";

      BUNDLE_USER_CONFIG = "${configHome}/bundle";
      BUNDLE_USER_CACHE = "${cacheHome}/bundle";
      BUNDLE_USER_PLUGIN = "${dataHome}/bundle";

      RUSTUP_HOME = "${dataHome}/rustup";
      CARGO_HOME = "${dataHome}/cargo";

      NPM_CONFIG_USERCONFIG = "${configHome}/npm/npmrc";

      GHCUP_USE_XDG_DIRS = "true";
      CABAL_CONFIG = "${config.xdg.configHome}/cabal/config";
      CABAL_DIR = "${config.xdg.dataHome}/cabal";

      PSQL_HISTORY = "${config.xdg.dataHome}/psql_history";
      GRADLE_USER_HOME = "${config.xdg.dataHome}/gradle";
      ANDROID_USER_HOME = "${config.xdg.dataHome}/android";
      GOPATH = "${config.xdg.dataHome}/go";

      MINT_PATH = "${config.xdg.dataHome}/mint";
      MINT_LINK_PATH = "${config.xdg.dataHome}/mint/bin";
    };
    xdg.configFile."npm/npmrc" = {
      text = ''
        cache=${config.xdg.cacheHome}/npm
        init-module=${config.xdg.configHome}/npm/init.js
      '';
    };
  };
}
