{
  config,
  lib,
  pkgs,
  ...
}:
{
  config.programs.fish.enable = lib.mkDefault true;
  config.environment.shells = lib.optionals (config.programs.fish.enable) [
    pkgs.fish
  ];
}
