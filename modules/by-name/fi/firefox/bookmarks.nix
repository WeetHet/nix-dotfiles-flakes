{ lib, config, ... }:
let
  mkBookmarks = lib.mapAttrsToList (
    name: url: {
      inherit name url;
    }
  );
in
{
  config = lib.mkIf config.programs.firefox.enable {
    programs.firefox.profiles.default.bookmarks = [
      {
        name = "toolbar";
        toolbar = true;
        bookmarks = mkBookmarks {
          "GitLab" = "https://gitlab.com";
          "Google" = "https://google.com";
          "GitHub" = "https://github.com";
          "Typst" = "https://typst.app";
          "Google Calendar" = "https://calendar.google.com";
          "Google Classroom" = "https://classroom.google.com";
          "XKCD" = "https://xkcd.com";
          "NixOS Search" = "https://search.nixos.org";
          "Noogle" = "https://noogle.dev";
        };
      }
    ];
  };
}
