{
  pkgs,
  lib,
  config,
  ...
}:
{
  imports = [ ./bookmarks.nix ];

  config = lib.mkIf config.programs.firefox.enable {
    home = lib.optionalAttrs pkgs.stdenv.hostPlatform.isDarwin {
      launchVariables.MOZ_LEGACY_PROFILES = 1;
    };

    programs.firefox = {
      package = if pkgs.stdenv.hostPlatform.isDarwin then pkgs.librewolf-unwrapped else pkgs.librewolf;
      configPath =
        if pkgs.stdenv.hostPlatform.isDarwin then "Library/Application Support/LibreWolf" else ".librewolf";
      profiles.default = {
        extensions.packages = with pkgs.nur.repos.rycee.firefox-addons; [
          ublock-origin
          bitwarden
          adaptive-tab-bar-colour
          consent-o-matic
          refined-github
        ];
        settings = {
          "app.update.auto" = false;
          "browser.aboutConfig.showWarning" = false;
          "browser.warnOnQuit" = false;
          "browser.theme.dark-private-windows" = true;
          "browser.toolbars.bookmarks.visibility" = "never";
          "browser.bookmarks.addedImportButton" = false;
          "browser.startup.page" = 3; # Restore previous session
          "browser.urlbar.suggest.trending" = false;
          "browser.urlbar.suggest.topsites" = false;
          "browser.aboutwelcome.didSeeFinalScreen" = true; # (Try to) disable welcome splash
          "trailhead.firstrun.didSeeAboutWelcome" = true;
          "dom.forms.autocomplete.formautofill" = false; # Disable autofill
          "signon.rememberSignons" = false; # Disable prompting to save passwords
          "extensions.formautofill.creditCards.enabled" = false; # Disable credit cards
          "dom.payments.defaults.saveAddress" = false; # Disable address save
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true; # Allow userChrome.css
          "extensions.autoDisableScopes" = 0;
          "extensions.pocket.enabled" = false;
          "browser.toolbarbuttons.introduced.pocket-button" = false;
        };
      };
      policies = {
        ExtensionSettings = {
          "uBlock0@raymondhill.net" = {
            default_area = "navbar";
          };
          "446900e4-71c2-419f-a6a7-df9c091e268b" = {
            default_area = "navbar";
          };
        };
      };
    };
  };
}
