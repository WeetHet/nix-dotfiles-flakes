{ pkgs, lib, ... }:
{
  config.home.packages = [ pkgs.mergiraf ];
  config.programs.git = {
    enable = lib.mkDefault true;
    userEmail = "stas.ale66@gmail.com";
    userName = "WeetHet";
    extraConfig = {
      init.defaultBranch = "main";
      push.autoSetupRemote = true;
      core.attributesFile = builtins.toString ./.gitattributes;
      "merge \"mergiraf\"" = {
        name = "mergiraf";
        driver = "mergiraf merge --git %O %A %B -s %S -x %X -y %Y -p %P -l %L";
      };
    };
    delta = {
      enable = true;
      options = {
        side-by-side = true;
        line-numbers = true;
      };
    };
  };
}
