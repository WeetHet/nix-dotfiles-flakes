{ inputs, ... }:
{
  config.nixpkgs = {
    config.allowUnfree = true;
    overlays = [
      inputs.nur.overlays.default
    ];
  };

  config.nix = {
    channel.enable = false;
    gc.automatic = true;
    optimise.automatic = true;
    settings = {
      sandbox = "relaxed";
      use-xdg-base-directories = true;
      experimental-features = "nix-command flakes ca-derivations";
      trusted-users = [
        "root"
        "@admin"
      ];
    };
    registry = {
      local.flake = inputs.self;
      nixpkgs.to = {
        owner = "NixOS";
        repo = "nixpkgs";
        rev = inputs.nixpkgs.rev;
        type = "github";
      };
    };
  };
}
