{ lib, ... }:
{
  config.programs.bat = {
    enable = lib.mkDefault true;
    config.theme = "TwoDark";
  };
}
