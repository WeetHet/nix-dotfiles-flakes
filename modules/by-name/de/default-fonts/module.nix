{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) types;
in
{
  options.defaultFonts = lib.mkOption {
    description = "List of default fonts to be installed in the system, which are used in the configs";
    type = types.listOf types.package;

    default = [
      pkgs.nerd-fonts.fira-code
      pkgs.monaspace
      pkgs.open-sans
      pkgs.victor-mono
      pkgs.inter
      pkgs.iosevka
    ];
  };

  config.fonts.packages = config.defaultFonts;
}
