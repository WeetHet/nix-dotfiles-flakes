{
  config,
  pkgs,
  lib,
  ...
}:
let
  inherit (lib) types;
in
{
  options.environment.defaultCasks = lib.mkOption {
    description = "List of default casks from homebrew to be installed";
    type = types.listOf types.package;

    default = [
      (pkgs.brewCasks.keyboardcleantool.overrideAttrs (old: {
        src = pkgs.fetchurl {
          inherit (old.src) url;
          hash = "sha256-QQXyeWOwiBo5mtdwgVQYt2HXqkEu1jadgkaAnCKDsK4=";
        };
      }))
      pkgs.iterm2
      pkgs.brewCasks.maccy
    ];
  };

  config = lib.mkIf pkgs.stdenv.isDarwin {
    home.packages = config.environment.defaultCasks;
  };
}
