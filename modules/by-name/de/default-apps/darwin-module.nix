{ config, lib, ... }:
let
  inherit (lib) types;
in
{
  options.environment.defaultMasApps = lib.mkOption {
    description = "List of default apps from mac app store to be installed";
    type = types.attrs;

    default = {
      "Telegram" = 747648890;
      Bitwarden = 1352778147;
      CotEditor = 1024640650;
    };
  };

  config.homebrew.masApps = config.environment.defaultMasApps;
  config.homebrew.casks = [ "orbstack" ];
}
