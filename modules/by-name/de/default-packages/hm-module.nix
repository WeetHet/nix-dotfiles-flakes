{
  lib,
  pkgs,
  ...
}:
{
  config = {
    programs.atuin.enable = lib.mkDefault true;
    programs.zoxide.enable = lib.mkDefault true;
    programs.home-manager.enable = lib.mkDefault true;
    programs.less.enable = true;

    home.packages = [
      pkgs.fd
      pkgs.ripgrep
      pkgs.fzf
      pkgs.nixd
      pkgs.nixfmt-rfc-style
      pkgs.devenv

      pkgs.docker
    ] ++ (lib.optional pkgs.stdenv.isDarwin pkgs.darwin.trash);
  };
}
