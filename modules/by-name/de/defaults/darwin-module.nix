{ ... }:
{
  config.security.pam.services.sudo_local.touchIdAuth = true;

  config.system.defaults = {
    NSGlobalDomain = {
      AppleInterfaceStyleSwitchesAutomatically = true;
      AppleMetricUnits = 1;
      ApplePressAndHoldEnabled = false;
      AppleShowAllExtensions = true;
      AppleICUForce24HourTime = true;
      AppleTemperatureUnit = "Celsius";
      "com.apple.sound.beep.feedback" = 1;
      NSNavPanelExpandedStateForSaveMode = true;
      NSNavPanelExpandedStateForSaveMode2 = true;
    };
    alf = {
      globalstate = 2;
      allowsignedenabled = 0;
      stealthenabled = 1;
    };
    dock = {
      magnification = true;
      mru-spaces = false;
      tilesize = 52;
      largesize = 83;
      wvous-br-corner = 14;
    };
    LaunchServices.LSQuarantine = false;
    loginwindow.GuestEnabled = false;
    menuExtraClock.Show24Hour = true;
    trackpad.TrackpadRightClick = true;
    CustomUserPreferences = {
      NSGlobalDomain.NSQuitAlwaysKeepsWindows = 1;
    };
  };
}
