{ config, lib, ... }:
{
  imports = [
    {
      config.programs.direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
    }
  ];
  config = lib.mkIf config.programs.direnv.enable {
    programs.direnv.config.global = {
      hide_env_diff = true;
      load_dotenv = true;
    };
  };
}
