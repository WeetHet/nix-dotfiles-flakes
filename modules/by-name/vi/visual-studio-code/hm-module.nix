{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) types;
  cfg = config.programs.vscode;
  defaultUserSettings = {
    "update.mode" = "none";
    "extensions.autoUpdate" = false;
    "editor.fontFamily" = "FiraCode Nerd Font";
    "editor.fontSize" = 14;
    "editor.fontLigatures" = true;
    "editor.minimap.renderCharacters" = false;
    "editor.renderLineHighlight" = "gutter";
    "editor.smoothScrolling" = true;
    "editor.unicodeHighlight.nonBasicASCII" = false;
    "editor.unicodeHighlight.ambiguousCharacters" = false;
    "editor.inlayHints.enabled" = "offUnlessPressed";

    "files.autoSave" = "onFocusChange";
    "explorer.confirmDelete" = false;

    "window.titleBarStyle" = "custom";
    "window.restoreFullscreen" = true;

    "git.autofetch" = true;
    "workbench.startupEditor" = "none";
    "extensions.ignoreRecommendations" = true;

    "debug.onTaskErrors" = "abort";
    "terminal.integrated.fontSize" = 13;
    "terminal.integrated.enableMultiLinePasteWarning" = "never";

    "nix.enableLanguageServer" = true;
    "nix.serverPath" = "nixd";
    "nix.formatterPath" = "nixpkgs-fmt";
  };

  defaultExtensions = with pkgs.vscode-extensions; [
    ms-python.python
    ms-python.vscode-pylance

    jnoortheen.nix-ide
    mkhl.direnv
  ];
in
{
  options.programs.vscode = {
    withDefaultExtensions = lib.mkOption {
      description = "Include default extensions";
      type = types.bool;
      default = true;
    };
    withDefaultSettings = lib.mkOption {
      description = "Include default settings";
      type = types.bool;
      default = true;
    };
  };

  config.programs.vscode = {
    package = pkgs.vscodium;
    profiles.default = {
      userSettings = lib.optionalString cfg.withDefaultSettings defaultUserSettings;
      enableUpdateCheck = false;
      extensions = lib.optionals cfg.withDefaultExtensions defaultExtensions;
    };
  };
}
