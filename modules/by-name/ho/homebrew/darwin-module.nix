{ inputs, ... }:
{
  config.homebrew = {
    onActivation = {
      cleanup = "uninstall";
      autoUpdate = true;
      upgrade = true;
    };
    global.autoUpdate = false;
  };
  config.nixpkgs.overlays = [ inputs.brew-nix.overlays.default ];
}
