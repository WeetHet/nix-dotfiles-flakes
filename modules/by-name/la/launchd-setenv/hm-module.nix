{
  config,
  lib,
  pkgs,
  ...
}:
let
  launchctl-setenv = lib.concatLists (
    lib.mapAttrsToList (name: val: [
      name
      (toString val)
    ]) config.home.launchVariables
  );
in
{
  options = {
    home.launchVariables = lib.mkOption {
      type = lib.types.attrsOf (
        lib.types.oneOf [
          lib.types.str
          lib.types.int
          lib.types.bool
        ]
      );
      default = { };
      description = "Environment variables to set at launch time (Darwin only)";
    };
  };

  config = lib.mkIf pkgs.stdenv.hostPlatform.isDarwin {
    home.activation.launchctlSetenv = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      $DRY_RUN_CMD /bin/launchctl setenv ${lib.concatStringsSep " " launchctl-setenv}
    '';
    launchd.agents.launchctl-setenv = {
      enable = true;
      config = {
        ProgramArguments = [
          "/bin/launchctl"
          "setenv"
        ] ++ launchctl-setenv;
        KeepAlive.SuccessfulExit = false;
        RunAtLoad = true;
      };
    };
  };
}
