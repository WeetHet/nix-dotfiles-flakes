{ ... }:
{
  config.programs.helix.languages = {
    use-grammars.except = [ "gemini" ];
    language-server = {
      rust-analyzer.config.check = {
        command = "clippy";
      };
    };
    language = [
      {
        name = "typst";
        language-servers = [ "tinymist" ];
        roots = [ "template.typ" ];
      }
    ];
  };
}
