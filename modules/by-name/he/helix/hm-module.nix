{ lib, config, ... }:

{
  imports = [
    ./keymap.nix
    ./typst.nix
    { config.programs.helix.enable = lib.mkDefault true; }
  ];
  config = lib.mkIf config.programs.helix.enable {
    home.sessionVariables.EDITOR = lib.mkDefault "hx";
    xdg.configFile."helix/themes/edge_neon.toml".source = ./edge_neon.toml;
    programs.helix = {
      settings = {
        theme = "edge_neon";
        editor = {
          indent-guides.render = true;

          bufferline = "always";

          cursor-shape = {
            insert = "bar";
            normal = "block";
            select = "underline";
          };

          color-modes = true;
          auto-save = true;

          lsp.display-messages = true;
        };
      };
    };
  };
}
