{ config, pkgs, ... }:

{
  home.username = "weethet";
  home.homeDirectory = "/home/weethet";

  imports = [
    ./zed-editor.nix
  ];
  
  programs.home-manager.enable = true;

  programs.fish.enable = true;
  xdg.enable = true;

  home = {
    sessionVariables = {
      EDITOR = "hx";

      GHCUP_USE_XDG_DIRS = "true";
      CABAL_CONFIG = "${config.xdg.configHome}/cabal/config";
      CABAL_DIR = "${config.xdg.dataHome}/cabal";

      PSQL_HISTORY = "${config.xdg.dataHome}/psql_history";
      GRADLE_USER_HOME = "${config.xdg.dataHome}/gradle";
      ANDROID_USER_HOME = "${config.xdg.dataHome}/android";
      GOPATH = "${config.xdg.dataHome}/go";

      MINT_PATH = "${config.xdg.dataHome}/mint";
      MINT_LINK_PATH = "${config.xdg.dataHome}/mint/bin";
    };
  };

  home.packages = [
    pkgs.nerd-fonts.fira-code
    pkgs.materialgram
    pkgs.firefox
  ];

  fonts.fontconfig.enable = true;

  programs.git.enable = true;
  programs.starship.enable = true;
  programs.helix.enable = true;
  programs.bat.enable = true;

  programs.zoxide = {
    enable = true;
    enableFishIntegration = true;
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  programs.eza = {
    enable = true;
    icons = "auto";
  };

  nix = {
    package = pkgs.nixVersions.stable;
    gc.automatic = true;
    settings = {
      use-xdg-base-directories = true;
      experimental-features = "nix-command flakes";
    };
  };

  programs.less.enable = true;

  home.stateVersion = "24.05";
}
