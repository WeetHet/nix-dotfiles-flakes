{ modules, ... }:
let
  homeApps = "/Users/weethet/Applications/Home Manager Apps";
in
{
  users.users.weethet = {
    name = "weethet";
    home = "/Users/weethet";
    description = "Stanislav (WeetHet) Alekseev";
  };

  home-manager.users.weethet.imports = modules.home ++ [ ./home.nix ];

  # TODO: these are essentially user specific settings that are not in users.users yet
  system.defaults.dock.persistent-apps =
    [
      "/System/Applications/Launchpad.app"
      # "/System/Volumes/Preboot/Cryptexes/App/System/Applications/Safari.app"
      "${homeApps}/LibreWolf.app"
      "/System/Applications/Mail.app"
      "/Applications/Telegram.app"
      "/Applications/Xcode.app"
      "/Applications/Zed Nightly.app"
    ]
    ++ builtins.map (app: "${homeApps}/${app}") [
      "iTerm2.app"
      "Vesktop.app"
      "Spotify.app"
    ];

  homebrew.enable = true;
  homebrew.casks = [
    "battle-net"
    "element"
  ];
  homebrew.masApps = {
    "Reeder Classic" = 1529445840;
    "HSE App X" = 1527320487;
    Boop = 1518425043;
    "Microsoft To Do" = 1274495053;
    Xcode = 497799835;
  };
}
