{ pkgs, vscode-extensions, ... }:
let
  extensions = vscode-extensions.extensions.${pkgs.stdenv.system};
  marketplace = extensions.vscode-marketplace;
  nixpkgs = pkgs.vscode-extensions;
in
{
  programs.vscode = {
    enable = false;

    profiles.default = {
      userSettings = {
        "files.associations" = {
          ".clangd" = "yaml";
        };
        "jupyter.runStartupCommands" = [
          "%load_ext autoreload"
          "%autoreload 2"
        ];
        "[python]" = {
          "editor.formatOnType" = true;
          "editor.defaultFormatter" = "ms-python.black-formatter";
        };
        "notebook.lineNumbers" = "on";
        "texpresso.command" = "texpresso";
      };

      extensions = [
        marketplace.myriad-dreamin.tinymist
        marketplace.orangex4.vscode-typst-sympy-calculator

        nixpkgs.ms-toolsai.jupyter
        nixpkgs.ms-toolsai.vscode-jupyter-slideshow
        nixpkgs.ms-toolsai.vscode-jupyter-cell-tags
        nixpkgs.ms-toolsai.jupyter-renderers
        nixpkgs.ms-toolsai.jupyter-keymap
      ];
    };
  };
}
