{ config, pkgs, ... }:
let
  casks = pkgs.brewCasks;
  steam = (
    casks.steam.overrideAttrs (old: {
      src = pkgs.fetchurl {
        inherit (old.src) url;
        hash = "sha256-X1VnDJGv02A6ihDYKhedqQdE/KmPAQZkeJHudA6oS6M=";
      };
    })
  );
in
{
  imports = [ ./vscode.nix ];
  services.macos-remap-keys = {
    enable = true;
    keyboard."Capslock" = "Backspace";
  };

  enableJDKs = [
    pkgs.jdk21
  ];

  programs.firefox.enable = true;
  home.packages = [
    pkgs.jetbrains.idea-ultimate
    pkgs.utm
    steam

    casks.obs
    pkgs.vlc-bin

    pkgs.vesktop
    pkgs.zoom-us

    pkgs.spotify
  ];

  programs.fish.userPaths = [
    "${config.home.homeDirectory}/.local/bin"
    "${config.home.homeDirectory}/.local/share/cargo/bin"
    "/opt/homebrew/bin"
  ];
}
