{ ... }:
{
  imports = [ ./weethet ];

  nixpkgs.hostPlatform = "aarch64-darwin";
  nix.enable = true;

  networking = {
    hostName = "wavelength";
    computerName = "Wavelength Macbook";
    localHostName = "wavelength";
  };
}
