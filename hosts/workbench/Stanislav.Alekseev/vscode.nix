{ pkgs, vscode-extensions, ... }:
let
  extensions = vscode-extensions.extensions.${pkgs.stdenv.system};
  nixpkgs-ext = pkgs.vscode-extensions;
in
{
  programs.vscode = {
    enable = false;
    userSettings = {
      "dafny.version" = "custom";
      "dafny.verificationTimeLimit" = 100;

      "dafny.cliPath" = "${pkgs.dafny}/bin/dafny";
      "dafny.dotnetExecutablePath" = "${pkgs.dotnet-sdk}/bin/dotnet";
    };
    extensions = [
      extensions.vscode-marketplace.dafny-lang.ide-vscode

      nixpkgs-ext.ms-toolsai.jupyter
      nixpkgs-ext.ms-toolsai.vscode-jupyter-slideshow
      nixpkgs-ext.ms-toolsai.vscode-jupyter-cell-tags
      nixpkgs-ext.ms-toolsai.jupyter-renderers
      nixpkgs-ext.ms-toolsai.jupyter-keymap
    ];
  };
}
