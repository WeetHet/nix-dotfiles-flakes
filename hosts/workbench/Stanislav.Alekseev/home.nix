{ config, pkgs, ... }:
let
  casks = pkgs.brewCasks;
in
{
  services.macos-remap-keys = {
    enable = true;
    keyboard."Capslock" = "Backspace";
  };

  programs.firefox.enable = true;
  home.packages = [
    pkgs.jetbrains.idea-ultimate
    casks.maccy

    pkgs.vesktop
    pkgs.zoom-us
    pkgs.slack
    pkgs.spotify
  ];

  home.sessionVariables.DOTNET_ROOT = "${pkgs.dotnet-sdk}";
  programs.fish.userPaths = [
    "${config.home.homeDirectory}/.local/share/cargo/bin"
  ];

  enableJDKs = [
    pkgs.jdk21
  ];
}
