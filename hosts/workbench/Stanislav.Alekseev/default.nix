{ modules, ... }:
let
  homeApps = "/Users/Stanislav.Alekseev/Applications/Home Manager Apps";
in
{
  nixpkgs.hostPlatform = "aarch64-darwin";

  users.users."Stanislav.Alekseev" = {
    name = "Stanislav.Alekseev";
    home = "/Users/Stanislav.Alekseev";
    description = "Stanislav (WeetHet) Alekseev";
  };

  home-manager.users."Stanislav.Alekseev".imports = modules.home ++ [ ./home.nix ];

  system.defaults.dock.persistent-apps =
    [
      "/System/Applications/Launchpad.app"
      # "/System/Volumes/Preboot/Cryptexes/App/System/Applications/Safari.app"
      "${homeApps}/LibreWolf.app"
      "/System/Applications/Mail.app"
      "/Applications/Telegram.app"
      "/Applications/Zed Nightly.app"
    ]
    ++ builtins.map (app: "${homeApps}/${app}") [
      "Intellij IDEA.app"
      "iTerm2.app"
      "Slack.app"
      "Vesktop.app"
      "Spotify.app"
    ];

  homebrew.enable = true;
  homebrew.casks = [
    "battle-net"
    "element"
  ];
}
