{ ... }:
{
  imports = [ ./Stanislav.Alekseev ];

  nixpkgs.hostPlatform = "aarch64-darwin";
  nix.enable = true;

  networking = {
    hostName = "workbench";
    computerName = "WeetHet's Work Macbook";
    localHostName = "workbench";
  };
}
