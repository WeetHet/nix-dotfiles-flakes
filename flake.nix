{
  description = "WeetHet's darwin system flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    vscode-extensions = {
      url = "github:nix-community/nix-vscode-extensions";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    brew-nix = {
      url = "github:BatteredBunny/brew-nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nix-darwin.follows = "darwin";
      inputs.brew-api.follows = "brew-api";
      inputs.flake-utils.follows = "flake-utils";
    };
    brew-api = {
      url = "github:BatteredBunny/brew-api";
      flake = false;
    };
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs =
    {
      self,
      darwin,
      home-manager,
      ...
    }@inputs:
    let
      inherit (inputs.nixpkgs) lib;
      modules = import ./modules/top-level/all-modules.nix { inherit lib; };
      mkDarwinConfiguration =
        name:
        darwin.lib.darwinSystem {
          modules = [
            inputs.lix-module.nixosModules.default
            home-manager.darwinModules.home-manager
            ./hosts/${name}
          ] ++ modules.darwin;
          specialArgs = {
            inherit inputs modules name;
          };
        };
      mkHomeConfiguration =
        { name, system }:
        home-manager.lib.homeManagerConfiguration {
          pkgs = import inputs.nixpkgs { inherit system; };
          modules = [ ./hosts/${name} ] ++ modules.home;

          extraSpecialArgs = inputs;
        };
    in
    {
      lib = {
        withAllSystems = lib.genAttrs lib.systems.flakeExposed;
        isDarwin = system: lib.elem system lib.systems.doubles.darwin;
        forAllSystems =
          function: (self.lib.withAllSystems (system: function inputs.nixpkgs.legacyPackages.${system}));
      };

      darwinConfigurations = lib.flip lib.genAttrs mkDarwinConfiguration [
        "wavelength"
        "workbench"
      ];

      homeConfigurations = {
        wrathful-hero = mkHomeConfiguration {
          name = "wrathful-hero";
          system = "aarch64-linux";
        };
      };

      legacyPackages = self.lib.withAllSystems (
        system:
        import inputs.nixpkgs {
          inherit system;
          overlays = (if self.lib.isDarwin system then [ inputs.brew-nix.overlays.default ] else [ ]);
        }
      );

      formatter = self.lib.forAllSystems (pkgs: pkgs.nixfmt-rfc-style);
    };
}
