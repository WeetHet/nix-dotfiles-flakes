#!/usr/bin/env swift

import Foundation

class ScreenUnlockObserver {
    var homeDirectory = FileManager.default.homeDirectoryForCurrentUser

    init() {
        let dnc = DistributedNotificationCenter.default()

        let _ = dnc.addObserver(
            forName: NSNotification.Name("com.apple.screenIsUnlocked"),
            object: nil, queue: .main
        ) { _ in self.screenIsUnlocked() }

        RunLoop.main.run()
    }

    func screenIsUnlocked() {
        NSLog("Screen Unlocked")
        let task = Process()
        task.executableURL = URL(filePath: "/usr/bin/env")
        task.arguments = ["python3", homeDirectory.appending(components: ".local", "share", "emergency", "emergency_msg_update.py").path()]
        task.launch()
        task.waitUntilExit()
    }
}

let _ = ScreenUnlockObserver()
