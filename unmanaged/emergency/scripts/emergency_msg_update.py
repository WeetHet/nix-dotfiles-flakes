from telethon import TelegramClient, functions
from datetime import datetime, timedelta
import pathlib

config_path = pathlib.Path.home() / '.config' / 'emergency_fixup'
try:
    with open(config_path / 'api', 'r') as f:
        api_id = int(f.readline().strip())
        api_hash = f.readline().strip()
        emergency_chat = int(f.readline().strip())
except FileNotFoundError:
    print(f'File not found: {config_path / "api"}')
    exit(1)

client = TelegramClient(str(config_path / 'emergency.session'), api_id, api_hash)

async def main():
    while True:
        print("Trying to update emergency message...")
        try:
            chat = await client.get_input_entity(emergency_chat)
            scheduled = await client(functions.messages.GetScheduledHistoryRequest(peer=chat, hash=0))
            messages = scheduled.messages # type: ignore
            for message in messages:
                if message.message == "Test message":
                    print("Found test message, removing...")
                    await client(functions.messages.DeleteScheduledMessagesRequest(
                        peer=chat,
                        id=[message.id]
                    ))
            await client.send_message(chat, "Test message", schedule=datetime.utcnow() + timedelta(minutes=5))
            break
        except ValueError:
            print('ValueError, retrying...')
            await client.get_dialogs()

with client:
    client.loop.run_until_complete(main())
