echo "Installing Homebrew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo "Installing lix"
curl -sSf -L https://install.lix.systems/lix | sh -s -- install

echo "Removing /etc/nix/nix.conf"
sudo rm /etc/nix/nix.conf

echo "Bootstrapping nix-darwin"
export PATH=/opt/homebrew/bin:$PATH
nix --experimental-features 'nix-command flakes' run nix-darwin -- switch --flake .#wavelength

echo "Setting correct shell"
